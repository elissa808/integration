package org.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void add() { assertEquals(4, Calculator.add(2, 2), 0.01); }

    @Test
    public void subtract() { assertEquals(8, Calculator.subtract(10, 2), 0.01); }


    @Test
    public void multiply() { assertEquals(8, Calculator.multiply(4, 2), 0.01); }


    @Test
    public void divide() { assertEquals(3, Calculator.divide(9, 3), 0.01); }

}