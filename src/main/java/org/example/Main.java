package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        double num1, num2;

        // Take input from the user
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the numbers:");

        // Take the inputs
        num1 = sc.nextDouble();
        num2 = sc.nextDouble();

        System.out.println("Enter the operator (+,-,*,/):");

        char op = sc.next().charAt(0);

        if (op == '+'){
            System.out.println(num1 + " " + op + " " + num2 + " = " + Calculator.add(num1, num2));
        } else if (op == '-'){
            System.out.println(num1 + " " + op + " " + num2 + " = " + Calculator.subtract(num1, num2));
        } else if (op == '*'){
            System.out.println(num1 + " " + op + " " + num2 + " = " + Calculator.multiply(num1, num2));
        } else if (op == '/'){
            System.out.println(num1 + " " + op + " " + num2 + " = " + Calculator.divide(num1, num2));
        } else {
            System.out.println("Invalid operator");
        }


    }
}